bash:
	docker-compose exec elasticsearch bash

ping:
	curl 127.0.0.1:9200

health:
	curl 127.0.0.1:9200/_cat/health

nodes:
	curl -X GET "localhost:9200/_cat/nodes?v"

indices:
	curl 127.0.0.1:9200/_cat/indices?v

