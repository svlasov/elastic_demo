## Resources
* [Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)

## Structured Logging
* [**Structured Logging with Filebeat!**](https://www.elastic.co/blog/structured-logging-filebeat)
* [Python Logstash Formatter](https://github.com/ulule/python-logstash-formatter)
* [structlog](https://github.com/hynek/structlog)
    * [logging & structlog](http://www.structlog.org/en/stable/standard-library.html#suggested-configurations)
* [Python JSON Logger](https://github.com/madzak/python-json-logger)
* [simplejson](https://pypi.org/project/simplejson/)
* [Python Logging with JSON Steroids](https://logmatic.io/blog/python-logging-with-json-steroids/)
* [Django Request & Response Objects](https://docs.djangoproject.com/en/2.0/ref/request-response/)
* [Django View Decorators](https://docs.djangoproject.com/en/2.0/topics/http/decorators/)


### Courses
* [LinuxAcademy course](https://linuxacademy.com/cp/modules/view/id/193)
* [Udemy couse](https://www.udemy.com/elasticsearch-6-and-elastic-stack-in-depth-and-hands-on/learn/v4/content)
    * [Udemy Course Resources](http://sundog-education.com/elasticsearch/)
        * [PDF file](http://media.sundog-soft.com/es6/ElasticStack.pdf)

### Books
* [Learning Elastic Stack 6.0](https://www.packtpub.com/mapt/book/all_books/9781787281868)

### Install the Shakespeare Search Index in Elasticsearch 6:
```
wget http://media.sundog-soft.com/es6/shakes-mapping.json
```

```
curl -H 'Content-Type: application/json' -XPUT 127.0.0.1:9200/shakespeare --data-binary @shakes-mapping.json
```

```
wget http://media.sundog-soft.com/es6/shakespeare_6.0.json
```

```
curl -H 'Content-Type: application/json' -X POST 'localhost:9200/shakespeare/doc/_bulk?pretty' --data-binary  @shakespeare_6.0.json
```

```
curl -H 'Content-Type: application/json' -XGET '127.0.0.1:9200/shakespeare/_search?pretty' -d '
{
"query" : {
"match_phrase" : {
"text_entry" : "to be or not to be"
}
}
}
'
```
