import calendar
import logging
import sys
import time

import structlog
from structlog import PrintLogger, wrap_logger
from structlog.processors import JSONRenderer

logging.basicConfig(
    format="%(message)s",
    stream=sys.stdout,
    level=logging.DEBUG,
)

structlog.configure(
    wrapper_class=structlog.stdlib.BoundLogger,
)

def timestamper(logger, log_method, event_dict):
    event_dict["timestamp"] = calendar.timegm(time.gmtime())
    return event_dict

wrapped_logger = logging.getLogger(__name__) #PrintLogger()

f1 = timestamper
f2 = JSONRenderer()

log_wrapper = wrap_logger(wrapped_logger, processors=[f1, f2])
modified_log_wrapper = log_wrapper.new(x=42)

log_wrapper.debug("some_event", y=23)