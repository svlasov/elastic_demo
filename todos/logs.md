* create and provision a server
* run ElasticSearch docker cluster
* configure and run Filebeat
* automate server provisioning with Fabric/Ansible/Salt
* run elasticsearch cluster on k8s
