# Essentials

## Beats

* Filebeat
* Packetbeat
* Heartbeat
* Winlogbeat (Windows only)
* Auditbeat