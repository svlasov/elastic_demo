import json
import logging
import os

from structlog import wrap_logger
from structlog.processors import JSONRenderer

logfilename = os.path.abspath("/var/log/sdcc/sdcc-portal-logstash.log")
os.remove(logfilename)

logging.basicConfig(level=logging.DEBUG, filename=logfilename, format="%(msg)s")

logging_logger = logging.getLogger(__name__)

json_renderer = JSONRenderer()

log = wrap_logger(logging_logger, processors=[json_renderer])

log = log.bind(user='arthur', id=42, verified=False)
# log.debug('logged_in')
log.debug('changed_state', verified=True)

#logging_logger.debug(json.dumps({"aa": 55}))