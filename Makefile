# run this if thre is not enough virtual memory for elastic
prepare:
	sudo sysctl -w vm.max_map_count=262144

up:
	docker-compose up --build -d

down:
	docker-compose down

restart: down up

logs:
	docker-compose logs

ps:
	docker-compose ps

version:
	docker-compose version

check:
	docker-compose config

up_es:
	docker-compose up --build -d elasticsearch

run_ls:
	docker-compose run logstash

ls_sh:
	docker-compose exec logstash bash


es_ping:
	curl 127.0.0.1:9200

es_health:
	curl 127.0.0.1:9200/_cat/health

es_nodes:
	curl -X GET "localhost:9200/_cat/nodes?v"

es_indices:
	curl 127.0.0.1:9200/_cat/indices?v
